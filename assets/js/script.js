// Document ready
$(window).ready(function(){	
	console.log('Space on');
	global.init();
		
	// Setup planets
	global.planets['sun'] = new Planet(695500, 0, 0, 0, 25, 0, 0xFFFFFF);	
	global.planets['sun'].addTexture(baseURL + 'assets/images/sun-color.jpg');
	global.three.add(global.planets['sun']);	
	
	global.planets['mercury'] = new Planet(6051, 0, 0, 0, 25, 0.000000627);
	global.planets['mercury'].addOrbit(global.planets['sun'], 70006464, 0, 0.000000413);	
	global.planets['mercury'].addTexture(baseURL + 'assets/images/mercury-color.jpg');
	global.planets['mercury'].addBumpMap(baseURL + 'assets/images/mercury-bumpmap.jpg');
	global.three.add(global.planets['mercury']);
	
	global.planets['venus'] = new Planet(2439, 0, 0, 0, 25, .000000062);
	global.planets['venus'].addOrbit(global.planets['sun'], 108000000, 0, 0.000000162);	
	global.planets['venus'].addTexture(baseURL + 'assets/images/venus-color.jpg');
	global.planets['venus'].addBumpMap(baseURL + 'assets/images/venus-bumpmap.jpg');
	global.planets['venus'].addAtmosphere(new Atmosphere(global.planets['venus'], 100, 0.1));
	global.planets['venus'].atmosphere.addTexture(baseURL + 'assets/images/neutral-atmosphere.png');
	global.three.add(global.planets['venus']);
	
	global.planets['earth'] = new Planet(6378, 0, 0, 0, 25, 0.000036343);																		   
	global.planets['earth'].addOrbit(global.planets['sun'], 14960000, 0, 0.0000001);
	global.planets['earth'].addTexture(baseURL + 'assets/images/earth-color.jpg');
	global.planets['earth'].addBumpMap(baseURL + 'assets/images/earth-bumpmap.jpg');
	global.planets['earth'].mesh.castShadow = false;
	global.planets['earth'].mesh.receiveShadow = false;
	
	var earthAtmosphere = new Atmosphere(global.planets['earth'], 300, 0.1);
	earthAtmosphere.addTexture(baseURL + 'assets/images/earth-atmosphere.png', 0.25);
	
	global.planets['earth'].addAtmosphere(earthAtmosphere);
	global.three.add(global.planets['earth']);
	
	global.planets['moon'] = new Planet(1737, 0, 0, 0, 25, 0.000001211);																		   
	global.planets['moon'].addOrbit(global.planets['earth'], 384403, 0, 0.00003115079);
	global.planets['moon'].addTexture(baseURL + 'assets/images/moon-color.jpg');
	global.planets['moon'].addBumpMap(baseURL + 'assets/images/moon-bumpmap.jpg');
	global.planets['moon'].mesh.castShadow = false;
	global.planets['moon'].mesh.receiveShadow = false;
	global.three.add(global.planets['moon']);
	
	global.planets['mars'] = new Planet(3397, 0, 0, 0, 25, 0.000000053);
	global.planets['mars'].addOrbit(global.planets['sun'], 228526848, 0, .000000047);	
	global.planets['mars'].addTexture(baseURL + 'assets/images/mars-color.jpg', 1);
	global.planets['mars'].addBumpMap(baseURL + 'assets/images/mars-bumpmap.jpg');
	global.planets['mars'].addAtmosphere(new Atmosphere(global.planets['mars'], 200, 0.1));
	global.planets['mars'].atmosphere.addTexture(baseURL + 'assets/images/neutral-atmosphere.png');
		
	global.three.add(global.planets['mars']);
	
	global.planets['jupiter'] = new Planet(71492, 0, 0, 0, 25, 0.000000008);
	global.planets['jupiter'].addOrbit(global.planets['sun'], 741000000, 0, 0.000000091);	
	global.planets['jupiter'].addTexture(baseURL + 'assets/images/jupiter-color.jpg');
	global.planets['jupiter'].addAtmosphere(new Atmosphere(global.planets['jupiter'], 1000, 0.5));
	global.planets['jupiter'].atmosphere.addTexture(baseURL + 'assets/images/neutral-atmosphere.png');
	global.three.add(global.planets['jupiter']);
	
	global.planets['saturn'] = new Planet(60268, 0, 0, 0, 25, 0.000000003);
	global.planets['saturn'].addOrbit(global.planets['sun'], 1433449370, 0, 0.000000091);	
	global.planets['saturn'].addTexture(baseURL + 'assets/images/saturn-color.jpg');
	global.planets['saturn'].addRing(new Ring(global.planets['saturn'], 1000, 150, 250, 50, 200, 0.1));	
	global.planets['saturn'].addRing(new Ring(global.planets['saturn'], 1000, 450, 400, 50, 200, 0.01));
	global.three.add(global.planets['saturn']);
	
	
	global.planets['uranus'] = new Planet(25559, 0, 0, 0, 25, 0.000000001);
	global.planets['uranus'].addOrbit(global.planets['sun'], 2876679082, 0, 0.000000098);	
	global.planets['uranus'].addTexture(baseURL + 'assets/images/uranus-color.jpg');
	global.three.add(global.planets['uranus']);
	
	global.planets['neptune'] = new Planet(24764, 0, 0, 0, 25, 0.000000001);
	global.planets['neptune'].addOrbit(global.planets['sun'], 4503443661, 0, 0.00000022);	
	global.planets['neptune'].addTexture(baseURL + 'assets/images/neptune-color.jpg');
	global.three.add(global.planets['neptune']);
	
	global.planets['pluto'] = new Planet(1180, 0, 0, 0, 25, 0.0000000004);
	global.planets['pluto'].addOrbit(global.planets['sun'], 5906376272, 0, 0.000000099);	
	global.planets['pluto'].addTexture(baseURL + 'assets/images/pluto-color.jpg');
	global.planets['pluto'].addBumpMap(baseURL + 'assets/images/pluto-bumpmap.jpg');
	global.three.add(global.planets['pluto']);
	
	global.stars = new Stars(100000000000, 25, baseURL + 'assets/images/stars.png');
	global.three.add(global.stars);
	
	global.planetsDict = Object.keys(global.planets);
	global.currentPlanet = 1;	
	global.focusPlanet = global.planets[global.planetsDict[global.currentPlanet]];
	
	$(this).keydown(function(e){
		var rotationSpeed = 0.1;
		switch(e.keyCode){
		case 38:// top
			global.three.camera.rotation.z -= rotationSpeed;
			break;
		case 37:// left
			global.focusOnPlanet(-1);		
			break;
		case 39:// right
			global.focusOnPlanet(1);
			break;
		case 40: // bottom
			global.three.camera.rotation.z += rotationSpeed;
			break;
		case 187: // +
			global.three.cameraZ += 1000 * global.scale;
			break;
		case 189: // -
			global.three.cameraZ -= 1000 * global.scale;
			break;
		}
		
	});

	
	global.start();
	
	$(window).scroll(function(e){
		console.log(e);		
	});
	
	$(window).mousedown(function(e){
		global.mouse.down = true
		global.mouse.startX = e.pageX;
		global.mouse.startY = e.pageY;
		
		$(window).mousemove(function(e){
			  global.mouse.x = e.pageX;
		      global.mouse.y = e.pageY;
		}); 
		
		$(window).mouseup(function(e){
			global.mouse.lastX = global.three.cameraX;
			global.mouse.lastY = global.mouse.startY + global.mouse.y;
			
			global.mouse.down = false;
			
			$(window).unbind('mousemove');
			$(window).unbind('mouseup');
			
			console.log(global.mouse.x);
			console.log(global.mouse.y);
		});
	});
});

var three = {
		camera : null,
		cameraX : null,
		cameraY : null,
		cameraZ : 1000,
		controls : null,
		scene : null,
		renderer : null,
		composer : null,
		renderables : null,
		init : function(){
			this.camera = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 1, 1000000000 );
			this.camera.position.z = this.cameraZ;
			
			this.scene = new THREE.Scene();			
			
			this.renderer = new THREE.WebGLRenderer();
			this.renderer.autoClear = false;
			this.renderer.shadowMapEnabled = true;
			this.renderer.setSize( window.innerWidth, window.innerHeight );
			
			this.renderables = [];
			
			var ambient = new THREE.AmbientLight( 0xCCCCCC );
			this.scene.add( ambient );


			pointLight = new THREE.PointLight( 0x999999 );
			this.scene.add( pointLight );
			
			// effects
			var renderModel = new THREE.RenderPass( this.scene, this.camera );
			var effectBloom = new THREE.BloomPass( 2.0 );
			var effectScreen = new THREE.ShaderPass( THREE.ShaderExtras[ "screen" ] );

			effectScreen.renderToScreen = true;

			this.composer = new THREE.EffectComposer( this.renderer );

			this.composer.addPass( renderModel );
			this.composer.addPass( effectBloom );
			this.composer.addPass( effectScreen );
			
			
			document.body.appendChild( this.renderer.domElement );
		},
		add : function(renderable){
			this.renderables.push(renderable);
			this.scene.add(renderable.mesh);
		},
		render : function(){
			// this.renderer.render( this.scene, this.camera );
			
			this.renderer.clear();
			this.composer.render( 0.1 );
			
			if(global.mouse.down){		      
			      global.three.cameraX += ( global.mouse.x - global.mouse.startX ) * global.panSpeed;
			      global.three.cameraY += ( global.mouse.y - global.mouse.startY ) * global.panSpeed;
			}
			
			$.each(this.renderables, function(index,renderable){
				renderable.render();
			});		
			
		    var x = Math.ceil(Math.sin( global.three.cameraX / 1000 ) * (global.three.cameraZ));
		    var y = Math.sin((Math.PI) * (global.three.cameraY / 10000)) * ( global.three.cameraZ);
	    	var z = Math.ceil(Math.cos( global.three.cameraX / 1000 ) * (global.three.cameraZ));
	    	
			global.three.camera.position.x = ( x ) + global.focusPlanet.mesh.position.x;
			global.three.camera.position.y = ( y ) + global.focusPlanet.mesh.position.y;
			global.three.camera.position.z = z + global.focusPlanet.mesh.position.z;
			
			global.three.camera.lookAt( global.focusPlanet.mesh.position );
		}
};


console.log(window);

var global = {
		three : three,
		mouse : {
			down : false,
			startX : null,
			lastX : null,
			x : null,
			startY : null,
			lastY : null,
			y : null
		},
		time : null,
		panSpeed : .1,
		scale : .01,
		speed : 1,
		planets : {},
		focusPlanet : 'earth',
		planetsDict : null,
		stars : null,
		init : function(){
			this.three.init();
			//this.guiInit();
		},
		start : function(){
			this.render();
		},
		render : function(){
			this.time = (new Date().getTime() / 1000) * global.speed;
		
			this.three.render();

			requestAnimationFrame( $.proxy(this.render, this) );
		},
		guiInit : function(){
			var gui = new DAT.GUI();
			gui.add(global, 'speed');
			gui.add(global, 'focusPlanet', global.planetsDict);
		},
		focusOnPlanet : function(count){
			global.focusPlanet.active = false;
			
			global.currentPlanet += count;
			if(global.currentPlanet < 0){
				global.currentPlanet = global.planetsDict.length - 1;
			} else if(global.currentPlanet == global.planetsDict.length){
				global.currentPlanet = 0;
			}
			
			global.focusPlanet = global.planets[global.planetsDict[global.currentPlanet]];
			global.focusPlanet.active = true;
			global.three.cameraZ = global.focusPlanet._width * 8;
		}
}

var Renderable = Base.extend({
	mesh : null,
	material : null,
	active : null,
	constructor : function(){
		console.log('New renderable');
	},
	render : function(){
		// Extend
	}
});

var Stars = Renderable.extend({
	constructor : function(width, segments, srcPath){
	    var geometry = new THREE.SphereGeometry( width * global.scale, segments, segments );
	    this.material = new THREE.MeshBasicMaterial({
	    	ambient: 0xCCCCCC,
	    	map : THREE.ImageUtils.loadTexture(srcPath),
	    	side : THREE.DoubleSide
	    });
	    
	    this.material.map.wrapS = this.material.map.wrapT = THREE.RepeatWrapping;
	    this.material.map.repeat.set(1,1);
	    this.mesh = new THREE.Mesh(geometry, this.material);
	    
	    console.log(this.material);
	}
});

/**
 * rps Rotation per second
 */
var Planet = Renderable.extend({
	_width : null,
	_x : null,
	_y : null,
	_z : null,
	_segments : null,
	_rps : null,
	mesh : null,
	material : null,
	orbiting : null,
	orbitParent : null,
	orbitX : null,
	orbitY : null,
	timeOffset : null,
	atmosphere : null,
	rings : null,
	constructor : function(width, x, y, z, segments, rps, ambient){				
		this._width = Math.ceil(width * global.scale);
		this._x = Math.ceil(x * global.scale);
		this._y = Math.ceil(y * global.scale);
		this._z = Math.ceil(z * global.scale);
		this._segments = segments;
		this._rps = rps;
		
		if(!ambient){
			ambient = 0x202020;
		}
		
	    var geometry = new THREE.SphereGeometry( this._width, this._segments, this._segments );
	    this.material = new THREE.MeshPhongMaterial({
	    	ambient: ambient,
	    	specular: 0x222222, 
	    	shininess: 0.0
	    });
	    
	    this.mesh = new THREE.Mesh(geometry, this.material);
	    this.mesh.position.set(this._x, this._y, this._z);	
	    
	    this.timeOffset = 0;
	    this.orbiting = false;
	    this.rings = [];
	},
	addTexture : function(srcPath, fullyLid){		
		this.material.map = THREE.ImageUtils.loadTexture(srcPath);
	},
	addBumpMap : function(srcPath){
		this.material.bumpMap = THREE.ImageUtils.loadTexture(srcPath);
	},
	addOrbit : function(parent, x, y, speed){
		this.orbitParent = parent;
		this.orbitX = Math.ceil(x * global.scale);
		this.orbitY = y;
		this.orbitSpeed = speed;
		this.orbiting = true;
	},
	addAtmosphere : function(atmosphere){
		this.atmosphere = atmosphere;
		global.three.scene.add(this.atmosphere.mesh);
	},
	addRing : function(ring){
		this.rings.push(ring);
		global.three.scene.add(ring.mesh);
	},
	render : function(){				
	    if(this.orbiting){
	    	this.mesh.position.x = Math.ceil(this.orbitParent.mesh.position.x + Math.sin((global.time + this.timeOffset) * this.orbitSpeed)* (this.orbitParent._width + this._width + this.orbitX));
	    	this.mesh.position.z = Math.ceil(this.orbitParent.mesh.position.z + Math.cos((global.time + this.timeOffset) * this.orbitSpeed)* (this.orbitParent._width + this._width + this.orbitX));
	    }
	    
	    if(this.active){
	    	this.mesh.rotation.y = ( global.time * this._rps );
	  	    
	  	    if(this.atmosphere){
	  	    	this.atmosphere.render();
	  	    }
	  	    
	  	    if(this.rings.length != 0){
	  	    	$.each(this.rings, function(index,ring){
	  	    		ring.render();
	  	    	});
	  	    }
	    }
	  
	}
});


var Ring = Renderable.extend({
	_rps : null,
	parent : null,
	constructor : function(parent, objectCount, offsetMin, offsetMax, objectMinSize, objectMaxSize, rps){
		this.parent = parent;
		this._rps = rps;
		
		this.mesh = new THREE.Object3D();
		
		var objectMaterial = new THREE.MeshBasicMaterial({
			color : 0xCCCCCC
		});
		
		
		while(objectCount--){
			var objectWidth = (objectMinSize + (objectMaxSize*Math.random())) * global.scale;
			
			var objectGeography = new THREE.SphereGeometry( objectWidth , 10, 10 );			
			var objectMesh = new THREE.Mesh(objectGeography,objectMaterial);
			var objectRandom = 1000 * Math.random();
			var offset = offsetMin + (offsetMax * Math.random());
			objectMesh.position.x = Math.sin(objectRandom) * (this.parent._width + objectWidth + offset); //Math.ceil(0 + Math.sin(100 * Math.random()) * (this.parent._width + this.offset));
			objectMesh.position.z = Math.cos(objectRandom) * (this.parent._width + objectWidth + offset); //Math.ceil(0 + Math.cos(100 * Math.random()) * (this.parent._width + this.offset));
			
			this.mesh.add(objectMesh);
		}
	},
	render : function(){		
		this.mesh.rotation.y = ( global.time * this._rps ) + this.parent.mesh.rotation.y;
		this.mesh.position.copy(this.parent.mesh.position);
	}
});


var Atmosphere = Renderable.extend({
	_rps : null,
	_width : null,
	parent : null,
	constructor : function(parent, width, rps, ambient){
		this.parent = parent;
		this._width = ( width * global.scale ) + this.parent._width;
		this._rps = rps;
		
		if(!ambient){
			ambient = 0x404040;
		}
		
	    var geometry = new THREE.SphereGeometry( this._width, this.parent._segments, this.parent._segments );
	    this.material = new THREE.MeshPhongMaterial({
	    	ambient: ambient,
	    	transparent : true
	    });
	    
	    this.mesh = new THREE.Mesh(geometry, this.material);
	    this.mesh.position.copy(this.parent.mesh.position);
	},
	addTexture : function(srcPath, opacity){
		if(!opacity){
			opacity = 0.5;
		}
		
		this.material.map = THREE.ImageUtils.loadTexture(srcPath);
		this.material.opacity = opacity;
		this.material.map.repeat.set(1,1);
	},
	addBumpMap : function(srcPath){
		this.material.bumpMap = THREE.ImageUtils.loadTexture(srcPath);
	},
	render : function(){		
		this.mesh.rotation.y = ( global.time * this._rps ) + this.parent.mesh.rotation.y;
		this.mesh.rotation.x = ( global.time * this._rps ) + this.parent.mesh.rotation.y;
		this.mesh.position.copy(this.parent.mesh.position);
	}
});
